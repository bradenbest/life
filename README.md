# Conway's Game of Life

It's the Game of Life. Nothing really special about it. There are better, easier to use implementations out there. But I made one, so, cool.

This readme only focuses on the C++ version. The HTML version is super easy to get up and running because you only need a working modern internet browser with javascript. And I'll assume you have one since you're on this website.

# Configuration

Most configuration is done in `life.cpp`, `presets.cpp` and `presets.h`. Though there's some weird hard-coded stuff, like `fps` being defined unconditionally in `draw.cpp`, for example. Laziness.

# Creating a preset

See the readme in `cpp/` for info on that..

# Compilation

Here are some basic compile instructions to hopefully help you to compile this. If you don't follow the instructions carefully, or use some esoteric environment made for single-file projects or something (I'm looking at you, C4droid users), then don't complain when you can't get it to compile. 

To compile this, you will need:

* A C++ compiler
* A command line that isn't broken and/or stupid
* The SDL 2.0 library

Do not attempt to use an IDE to compile this unless you really know what you're doing. I made this project using nothing but a text editor and a compiler (as I do with all of my projects), hence why there's a hand-written makefile included.

If you're running a linux system with GNU Make and G++, then you're in luck, because I *also* run linux and have GNU Make and G++!

    $ make
    $ ./life

That's the build-and-run process in a nutshell, and there isn't much reason for it to go wrong, but it might...

If you're running a different system, chances are you're gonna have to dabble in the compiler. Usually, SDL will come with a program called `sdl-config` (or `sdl2-config`). Running that in the compile command should generate the correct compile line.

For example, this quick and dirty command line:

    g++ *.cpp $(sdl2-config --cflags --libs)

Expands to the following on my system:

    g++ *.cpp -I/usr/include/SDL -D_GNU_SOURCE=1 -D_REENTRANT -L/usr/lib/x86_64-linux-gnu -lSDL

It gives the necessary header and library archive paths to `#include` and link SDL properly.

If you run Windows, you're on your own. I'm sure you can figure it out, though. I mean you got `cmd.exe`, `powershell`, and hopefully something sane like `cygwin` handy. I'm sure you'll work something out.

# License

There is none. This is public domain. 

Though I do appreciate courteous people who mention the original creator when redistributing. I mean, if that's what you want to do.

-Braden
