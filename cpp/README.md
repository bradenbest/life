## How to make a preset

You can make a preset by adding it to `presets.cpp` and `presets.h`

An entry in `presets.h` must be an `extern const int []`.

The matching entry in `presets.cpp` must follow the following format:

    const int name[] = {
        length,
        array index,
        array index,
        array index,
        ...
    };

For example, a line of 5 live squares at the top left of the board might look like this:

    presets.h:
        extern const int line[];

    presets.cpp:
        const int line[] = { 5, 0, 1, 2, 3, 4 };

To use it, edit `life.cpp:Life::Life()`. 

    board->setPreset(presets::line);

For more elaborate presets, I've provided a macro called `XYCNTR(x, y)` which will activate a cell based on an (x,y) coodinate relative to the center of the board. So you could implement the `r-pentomino` (already in there), for example, as such:

    const int r_pentomino[] = {
        5,
        XYCNTR(-1,  0),
        XYCNTR( 0, -1),
        XYCNTR( 0,  0),
        XYCNTR( 0,  1),
        XYCNTR( 1, -1),
    };

It's easy. Just draw the pattern on paper or find a diagram of it, and then figure out where you want your origin (0,0) to be. In this case, I chose the middle most cell in the R-pentomino as my origin, and all coordinates are relative to that.

Treat it like an upside-down euclidian plotting graph and you're golden.

First, count the number of active cells

    const int r_pentomino[] = {
        5, 
    };

Then, what I like to do is go left-right/top-down. The left-most cell is 1 to the left, and 0 on the y axis. So the coordinate is (-1, 0).

    const int r_pentomino[] = {
        5,
        XYCNTR(-1, 0),
    };

The next cell is the cell above the origin, at (0, -1), then (0, 0), (0, 1), and finally, (1, -1)

    const int r_pentomino[] = {
        5,
        XYCNTR(-1,  0),
        XYCNTR( 0, -1),
        XYCNTR( 0,  0),
        XYCNTR( 0,  1),
        XYCNTR( 1, -1),
    };

You can set the fps to `FPS_STEP` to freeze the simulation before it begins, giving you a chance to look at the pattern to ensure you didn't make a mistake.

In `life.cpp:Life::Life()`:

    canvas->setFPS(FPS_STEP);

## Board size

This is an idea I had while designing the simulation.

The board's size could theoretically be multiplied by about 30 if I were to store a "layer" for each element in the board via bitmasking. That is, the right-most bit represents the base layer, and going left in the bits represents a further layer. So that when a given cell wraps from the right edge to the left, for example, 

    00000000 00000000 00000000 00000000

That's a 32 bit int. If I do this: `n |= (1 << 4)`, then the result will be

    00000000 00000000 00000000 00010000

So each bit could represent a different layer

    [1<<3] [1<<4] [1<<5]
    [1<<2] [1<<0] [1<<1] 
    [1<<6] [1<<7] [1<<8]

Or something along those lines. This would make the simulation significantly more complicated, but would add 31 "layers" to the board, where a live square on layer 1 wouldn't interfere with a live square on layer 2, for example. The squares would still be drawn in the same place, and at that point, I'd have to add some form of transparency to each square so you can tell when there's multiple squares being drawn in the same place. This would allow simulations to be much, much larger, but would complicate it as well.

I'm not doing that, but I thought it would be interesting to mention.

- - - -

## "Infinite" board

The board could be made infinite by holding a small, invisible, 3-wide and 3-high "padding" area surrounding the board. Once something steps beyond the padding area, it's deleted. This should theoretically give the simulation room to work when something gets to an edge of the board, such that the "offscreen" squares still affect onscreen squares. But then this raises problems with "bounce backs", patterns that explode in all directions that would be stopped short by this method, causing the simulation to be inaccurate.

Perhaps this could be combined with the above idea, except where only 9 bits out of a given cell are used, and once a cell steps beyond the border in a non-center layer, it gets deleted.

Either way, I'm sticking with periodic boundary conditions, which seems to be the preferred method from most GoL implementations I've played with.

- - - -

## On simulation accuracy and limitations

Keep in mind that no simulation of GoL is truly accurate. The spec calls for an infinite board, which can't exist in reality because computers have limited RAM. Boards with PBC can cause e.g. a gosper glider gun to destroy itself, and boards where cells at the perimeter are auto-killed are generally inaccurate because you'll get cells that die here, where in a larger board, they wouldn't die here. 

You might also get a configuration of 2x2 blocks at the edge caused by such conditions, and then, say, a glider runs into it at just the right trajectory and explodes backwards, breaking part of the simulation, where said glider would have otherwise not exploded and kept going, given a larger board.

The closest you can get to "infinite" is a 2D array that automatically expands in all four directions (`x`, `y`, `-x`, `-y`) when a cell manages to get there, accounting for negative indices and such...assuming the chosen language supports negative array indices, so Python and C are disqualified. Though C++ and JavaScript might work since C++ has that fancy `map<type, type>` deal, and JS uses hash tables to store all its objects (including arrays), meaning that `array[-1]` will allow you to store a value. However, it's slow because -1 isn't a valid index and JS has to convert it through some nebulous process that I'm unaware of. It takes much longer to access `array[-1]` than it does to access `array[1]`. So much for O(1) time complexity, huh.

Or if you want a **really** slow simulation, you could implement the board as a doubly linked list where each node has the x and y coodinate, and a cell state.

No matter how you implement this double-ended expanding array, this will, of course, break, when either the computer runs out of memory or the x or y coordinate reaches some 2 billion (or 9 quintillion if using a long). A 32-bit int is 4 bytes. Times 2 billion, that's 8 gigabytes. Most computers only have 1 - 4 GB of RAM, so if a glider reaching that coordinate and wrapping around to -2147483648 doesn't break the simulation, your computer grinding to a halt *will*.

- - - -

## An optimization that I neglected

The simulation would run a little bit faster if instead of copying board and tempboard back and forth, I alternated between them per generation. And I probably should have done that.

- - - -

## What the hell is going on with the game board?

I could use a 2-dimensional array (an array of arrays) for the board, but in my experience, 2D arrays are high maintenance and quite bug-prone if one is not careful, and much more annoying than 2D arrays in managed high level languages like JavaScript and Python. I chose to use a 1D array because it is simple.
