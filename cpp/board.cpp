#include <stdlib.h>

#include "draw.h"
#include "board.h"

Board::Board(int bwidth, int csize)
{/*{{{*/
    width = bwidth;
    size = width * width;
    board = (int *)calloc(sizeof(int), size);
    boardNext = (int *)calloc(sizeof(int), size);
    cellSize = csize;
}/*}}}*/

Board::~Board()
{/*{{{*/
    free(board);
    free(boardNext);
}/*}}}*/

void
Board::copy()
{/*{{{*/
    int i;

    for(i = 0; i < size; i++){
        board[i] = boardNext[i];
    }
}/*}}}*/

void
Board::draw(SDL *canvas)
{/*{{{*/
    int x,
        y;

    canvas->setColor(0, 0, 0);

    for(y = 0; y < width; y++){
        for(x = 0; x < width; x++){
            if(board[xytoi(x, y)]){
                canvas->fillRect(x * cellSize, y * cellSize, cellSize, cellSize);
            }
        }
    }

    canvas->update();
}/*}}}*/

int *
Board::getBoard()
{/*{{{*/
    return board;
}/*}}}*/

int
Board::getEast(int cell)
{/*{{{*/
    int target = cell + 1;

    if(target % width == 0){
        target = getNorth(target);
    }

    return target;
}/*}}}*/

int
Board::getNorth(int cell)
{/*{{{*/
    int target = cell - width;

    if(target < 0){
        target += size;
    }

    return target;
}/*}}}*/

int
Board::getSouth(int cell)
{/*{{{*/
    int target = cell + width;

    if(target >= size){
        target -= size;
    }

    return target;
}/*}}}*/

int *
Board::getTemp()
{/*{{{*/
    int i;

    for(i = 0; i < size; i++){
        boardNext[i] = board[i];
    }

    return boardNext;
}/*}}}*/

int
Board::getWest(int cell)
{/*{{{*/
    int target = cell - 1;

    if(target % width == width - 1 || target < 0){
        target = getSouth(target);
    }

    return target;
}/*}}}*/

int
Board::numAlive(int cell)
{/*{{{*/
    int alive = 0;
    int i;
    int neighbors[8] = {
        getNorth(cell),
        getNorth(getEast(cell)),
        getEast(cell),
        getSouth(getEast(cell)),
        getSouth(cell),
        getSouth(getWest(cell)),
        getWest(cell),
        getNorth(getWest(cell))
    };

    for(i = 0; i < 8; i++){
        if(board[neighbors[i]] == 1){
            alive++;
        }
    }

    return alive;
}/*}}}*/

void
Board::setPreset(const int *preset)
{/*{{{*/
    int i;

    for(i = 1; i < (*preset) + 1; i++){
        board[preset[i]] = 1;
    }
}/*}}}*/

int
Board::xytoi(int x, int y)
{/*{{{*/
    return x + (y * width);
}/*}}}*/
