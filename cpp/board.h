class SDL;

class Board {
    private:
        int *board;
        int *boardNext;
        int width;
        int size;
        int cellSize;

        int getNorth(int cell);
        int getEast(int cell);
        int getSouth(int cell);
        int getWest(int cell);

    public:
        Board(int bwidth, int csize);
        ~Board();

        void copy();
        void draw(SDL *canvas);
        int *getBoard();
        int *getTemp();
        int numAlive(int cell);
        void setPreset(const int *preset);
        int xytoi(int x, int y);
};
