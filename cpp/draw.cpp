#include <SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "draw.h"

SDL::SDL(int width, int height, const char *title)
{/*{{{*/
    if(SDL_Init(SDL_INIT_VIDEO) < 0)
        die();

    window = SDL_CreateWindow(title,
            SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
            width, height,
            SDL_WINDOW_SHOWN);

    if(window == NULL)
        die();

    screen = SDL_GetWindowSurface(window);
    color = SDL_MapRGB(screen->format, 0, 0, 0);
    bgcolor = SDL_MapRGB(screen->format, 255, 255, 255);
    fps = 10;
}/*}}}*/

SDL::~SDL()
{/*{{{*/
    SDL_FreeSurface(screen);
    SDL_DestroyWindow(window);
    SDL_Quit();
}/*}}}*/

void
SDL::clear()
{/*{{{*/
    SDL_FillRect(screen, 0, bgcolor);
}/*}}}*/

void
SDL::die()
{/*{{{*/
    fprintf(stderr, "%s\n", SDL_GetError());
    exit(1);
}/*}}}*/

void
SDL::fillRect(int x, int y, int w, int h)
{/*{{{*/
    SDL_Rect rect;

    rect.x = x;
    rect.y = y;
    rect.w = w;
    rect.h = h;
    SDL_FillRect(screen, &rect, color);
}/*}}}*/

const char *
SDL::getError()
{/*{{{*/
    return SDL_GetError();
}/*}}}*/

void
SDL::setColor(int r, int g, int b)
{/*{{{*/
    color = SDL_MapRGB(screen->format, r, g, b);
}/*}}}*/

void
SDL::setFPS(int newfps)
{/*{{{*/
    fps = newfps;
}/*}}}*/

void
SDL::sleep()
{/*{{{*/
    switch(fps){
        default:
            usleep(1000000 / fps);
            break;
        case FPS_STEP:
            fgetc(stdin);
            break;
        case FPS_UNLIMITED:
            break;
    }
}/*}}}*/

void
SDL::update()
{/*{{{*/
    SDL_UpdateWindowSurface(window);
}/*}}}*/
