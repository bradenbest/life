#define FPS_STEP 0
#define FPS_UNLIMITED -1

struct SDL_Window;
struct SDL_Surface;

class SDL {
    private:
        SDL_Window *window;
        SDL_Surface *screen;
        unsigned int color;
        unsigned int bgcolor;
        int fps;

        void die();
        const char *getError();

    public:
        SDL(int width, int height, const char *title);
        ~SDL();

        void clear();
        void fillRect(int x, int y, int w, int h);
        void setColor(int r, int g, int b);
        void setFPS(int newfps);
        void sleep();
        void update();
};
