#include <stdio.h>
#include <stdlib.h>
#include <SDL.h>

#include "draw.h"
#include "life.h"
#include "board.h"
#include "presets.h"

Life::Life(int bsize, int csize)
{/*{{{*/
    board_size = bsize;
    cell_size = csize;
    canvas_size = board_size * cell_size;
    canvas = new SDL(canvas_size, canvas_size, "Conway's Game of Life");
    board = new Board(board_size, cell_size);
    board->setPreset(presets::gosper_glider_gun);
    canvas->setFPS(30);
    generation = 1;
}/*}}}*/

Life::~Life()
{/*{{{*/
    delete canvas;
    delete board;
}/*}}}*/

void
Life::render()
{/*{{{*/
    canvas->clear();
    board->draw(canvas);
    printf("Generation: %i\r", generation);
    fflush(stdout);
    canvas->sleep();
}/*}}}*/

void
Life::run()
{/*{{{*/
    int selcell,
        alive,
        *tboard = board->getTemp(),
        *bboard = board->getBoard();

    render();

    for(selcell = 0; selcell < board_size * board_size; selcell++){
        alive = board->numAlive(selcell);

        if(bboard[selcell] == 1){
            if(alive < 2 || alive > 3){
                tboard[selcell] = 0;
            }
        } else {
            if(alive == 3){
                tboard[selcell] = 1;
            }
        }
    }

    board->copy();

    generation++;
}/*}}}*/
