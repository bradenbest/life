class SDL;
class Board;

class Life {
    private:
        SDL *canvas;
        Board *board;
        int board_size;
        int cell_size;
        int canvas_size;
        int generation;

        void render();

    public:
        Life(int border_size, int cell_size);
        ~Life();

        void run();
};
