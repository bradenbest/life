#include <SDL.h>

#include "life.h"
#include "presets.h"

int
main()
{
    Life *l = new Life(BOARD_SIZE, CELL_SIZE);
    SDL_Event e;
    bool running = true;

    while(running){
        while(SDL_PollEvent(&e)){
            if(e.type == SDL_QUIT || e.key.keysym.sym == SDLK_ESCAPE || e.key.keysym.sym == SDLK_q){
                running = false;
            }
        }

        l->run();
    }

    delete l;
    return 0;
}
