#include "presets.h"

namespace presets {

    const int blank[] = {0};

    const int acorn[] = {
        7,
        XYCNTR(-3,  1),
        XYCNTR(-2, -1),
        XYCNTR(-2,  1),
        XYCNTR( 0,  0),
        XYCNTR( 1,  1),
        XYCNTR( 2,  1),
        XYCNTR( 3,  1)
    };

    const int diehard[] = {
        7,
        XYCNTR(-3,  0),
        XYCNTR(-2,  0),
        XYCNTR(-2,  1),
        XYCNTR( 2,  1),
        XYCNTR( 3, -1),
        XYCNTR( 3,  1),
        XYCNTR( 4,  1)
    };

    const int glider[] = {
        5,
        XYCNTR( 0,  0),
        XYCNTR( 1,  0),
        XYCNTR( 2,  0),
        XYCNTR( 2, -1),
        XYCNTR( 1, -2)
    };

    const int gosper_glider_gun[] = {
        36,
        XYCNTR(-17,   0),
        XYCNTR(-17,   1),
        XYCNTR(-16,   0),
        XYCNTR(-16,   1),
        XYCNTR( -7,   0),
        XYCNTR( -7,   1),
        XYCNTR( -7,   2),
        XYCNTR( -6,  -1),
        XYCNTR( -6,   3),
        XYCNTR( -5,  -2),
        XYCNTR( -5,   4),
        XYCNTR( -4,  -2),
        XYCNTR( -4,   4),
        XYCNTR( -3,   1),
        XYCNTR( -2,  -1),
        XYCNTR( -2,   3),
        XYCNTR( -1,   0),
        XYCNTR( -1,   1),
        XYCNTR( -1,   2),
        XYCNTR(  0,   1),
        XYCNTR(  3,  -2),
        XYCNTR(  3,  -1),
        XYCNTR(  3,   0),
        XYCNTR(  4,  -2),
        XYCNTR(  4,  -1),
        XYCNTR(  4,   0),
        XYCNTR(  5,  -3),
        XYCNTR(  5,   1),
        XYCNTR(  7,  -4),
        XYCNTR(  7,  -3),
        XYCNTR(  7,   1),
        XYCNTR(  7,   2),
        XYCNTR( 17,  -2),
        XYCNTR( 17,  -1),
        XYCNTR( 18,  -2),
        XYCNTR( 18,  -1)
    };

    const int lightweight_spaceship[] = {
        9,
        XYCNTR(-2, -2),
        XYCNTR(-2,  0),
        XYCNTR(-1,  1),
        XYCNTR( 0,  1),
        XYCNTR( 1, -2),
        XYCNTR( 1,  1),
        XYCNTR( 2, -1),
        XYCNTR( 2,  0),
        XYCNTR( 2,  1),
    };

    const int r_pentomino[] = {
        5,
        XYCNTR( 0,  0),
        XYCNTR( 0, -1),
        XYCNTR( 0, -2),
        XYCNTR(-1, -1),
        XYCNTR( 1, -2)
    };

    const int spinner[] = {
        3,
        XYCNTR( 0,  0),
        XYCNTR( 1,  0),
        XYCNTR( 2,  0)
    };

};
