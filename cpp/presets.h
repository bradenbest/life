#define BOARD_SIZE 300
#define CELL_SIZE 3

#define XYCNTR(x, y) \
    ( (x + BOARD_SIZE/2) + (y + BOARD_SIZE/2) * BOARD_SIZE )

namespace presets {
    extern const int blank[];
    extern const int acorn[];
    extern const int diehard[];
    extern const int glider[];
    extern const int gosper_glider_gun[];
    extern const int lightweight_spaceship[];
    extern const int r_pentomino[];
    extern const int spinner[];
};
