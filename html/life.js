// TODO: implement the other cool ones from conway wiki, such as the diehard and acorn
var life = (function(options){
    var canvas,
        outtxt,
        canvas_size,
        ctx,
        board,
        board_size = options.board_size,
        board_center = board_size / 2,
        cell_size = options.cell_size,
        cell_padding = options.cell_padding,
        game_loop,
        generation,
        demos = {
            blank: [],
            glider: [
                xytoi(board_center + 0, board_center + 0),
                xytoi(board_center + 1, board_center + 0),
                xytoi(board_center + 2, board_center + 0),
                xytoi(board_center + 2, board_center - 1),
                xytoi(board_center + 1, board_center - 2)
            ],
            r_pentomino: [
                xytoi(board_center + 0, board_center + 0),
                xytoi(board_center + 0, board_center - 1),
                xytoi(board_center + 0, board_center - 2),
                xytoi(board_center - 1, board_center - 1),
                xytoi(board_center + 1, board_center - 2)
            ],
            spinner: [
                xytoi(board_center + 0, board_center + 0),
                xytoi(board_center + 1, board_center + 0),
                xytoi(board_center + 2, board_center + 0)
            ],
        };

    function init(preset){
        var i;

        canvas = document.getElementById("canvas");
        outtxt = document.getElementById("outtxt");
        ctx = canvas.getContext("2d");
        canvas_size = board_size * cell_size;
        canvas.width = canvas_size;
        canvas.height = canvas_size;
        board = [];
        generation = 0;

        for(i = 0; i < board_size * board_size; i++){
            board.push(0);
        }

        set_preset(demos[preset]);
    }

    function set_preset(preset){
        var i;

        for(i = 0; i < preset.length; i++){
            board[preset[i]] = 1;
        }
    }

    function draw_grid(){
        var i = 0;

        ctx.fillStyle = "#ccc";

        for(i = 0; i < board_size; i++){
            ctx.fillRect(i * cell_size, 0, 1, board_size * cell_size);
            ctx.fillRect(0, i * cell_size, board_size * cell_size, 1);
        }
    }

    function itoxy(n){
        return {
            x: Math.floor(n % board_size),
            y: Math.floor(n / board_size)
        };
    }

    function xytoi(x, y){
        return x + (y * board_size);
    }

    function get_north(cell){
        var target = cell - board_size;

        if(target < 0){
            target += board_size * board_size;
        }

        return target;
    }

    function get_east(cell){
        var target = cell + 1;

        if(target % board_size == 0){
            target = get_north(target);
        }

        return target;
    }

    function get_south(cell){
        var target = cell + board_size;

        if(target >= board_size * board_size){
            target -= board_size * board_size;
        }

        return target;
    }

    function get_west(cell){
        var target = cell - 1;

        if(target % board_size == board_size - 1 || target < 0){
            target = get_south(target);
        }

        return target;
    }

    function num_alive(cell){
        var neighbors,
            alive = 0,
            i;
        
        neighbors = [
            get_north(cell),
            get_north(get_east(cell)),
            get_east(cell),
            get_south(get_east(cell)),
            get_south(cell),
            get_south(get_west(cell)),
            get_west(cell),
            get_north(get_west(cell))
        ];

        for(i = 0; i < 8; i++){
            if(board[neighbors[i]] == 1){
                alive++;
            }
        }

        return alive;
    }

    function copy_array(array){
        var new_array = new Array(array.length),
            i;

        for(i = 0; i < array.length; i++){
            new_array[i] = array[i];
        }

        return new_array;
    }

    function tick(){
        var selcell,
            alive,
            temp_board = copy_array(board);

        for(selcell = 0; selcell < board_size * board_size; selcell++){
            alive = num_alive(selcell);

            if(board[selcell] == 1){
                if(alive < 2 || alive > 3){
                    temp_board[selcell] = 0;
                }
            } else {
                if(alive == 3){
                    temp_board[selcell] = 1;
                }
            }
        }

        board = copy_array(temp_board);
        outtxt.innerHTML = "generation: " + generation;
        generation++;
    }

    function draw_board(){
        var i = 0,
            coords;

        ctx.fillStyle = "#333";

        for(i = 0; i < board_size * board_size; i++){
            if(board[i]){
                coords = itoxy(i);
                ctx.fillRect(coords.x * cell_size + cell_padding, coords.y * cell_size + cell_padding, cell_size - cell_padding, cell_size - cell_padding);
            }
        }
    }

    function loop(){
        ctx.clearRect(0, 0, canvas_size, canvas_size);

        if(options.do_draw_grid)
            draw_grid();

        draw_board();
        tick();
    }

    function die(msg){
        clearInterval(game_loop);
        console.error(msg);
    }

    function main(){
        init("blank");
        game_loop = setInterval(loop, 1000 / options.fps);
    }

    return {
        main: main,
        init: init,
        die: die
    };
})

({
    board_size: 200,
    cell_size: 3,
    cell_padding: 0,
    do_draw_grid: 0,
    fps: 30,
});

window.onload = life.main;
